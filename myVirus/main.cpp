#define _WIN32_WINNT 0x0500
#include <iostream>
#include <Windows.h>
#include <thread>
#include <random>

#define SENSITIVITY 30
#define REACTION 7

std::thread createRunningPopUp(std::string msg, std::string title, RECT screenSize);
void createRunner(std::string msg, std::string title, RECT screenSize);
HWND genPopUpBox(std::string msg, std::string title, long mode);
std::thread movePopUp(HWND handle, std::string msg, std::string title, RECT screenSize);
void calcAndMoveWindow(HWND handle, LPRECT mergins, LPPOINT cursorPos, POINT winPosBottomRight, POINT winPosBottomLeft, POINT winPosTopRight, POINT winPosTopLeft, RECT screenSize);

int main()
{
	ShowWindow(GetConsoleWindow(), SW_HIDE);
	Sleep(15000);
	RECT desktop = {0};
	HWND desktopHandle = GetDesktopWindow();
	GetWindowRect(desktopHandle, &desktop);

	std::thread msg1 = createRunningPopUp("Hi There!!!", "Its Not A Virus!!", desktop);
	std::thread msg2 = createRunningPopUp("You Can't even close me....", "(Evil Laugh)", desktop);
	std::thread msg3 = createRunningPopUp("duddddddeeeee", "whasssupppppppppppp", desktop);
	std::thread msg4 = createRunningPopUp("if you close me i cry", "....", desktop);
	std::thread msg5 = createRunningPopUp("nooooooooooooo", "nooooo", desktop);
	std::thread msg7 = createRunningPopUp("You are a loserr", "Noderrr", desktop);

	
	msg1.join();
	msg2.join();
	msg3.join();
	msg4.join();
	msg5.join();
	//msg6.join();
	msg7.join();
	//
	//msg8.join();
	//msg9.join();
	//msg10.join();
	//msg11.join();
	//msg12.join();
	//msg13.join();
	//msg14.join();
	//
	return 0;
}

std::thread createRunningPopUp(std::string msg, std::string title, RECT screenSize)
{
	std::thread popUp(createRunner, msg, title, screenSize);
	return popUp;
}

void createRunner(std::string msg, std::string title, RECT screenSize)
{
	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> uniX(0, screenSize.right);
	std::uniform_int_distribution<int> uniY(0, screenSize.bottom);
	LPRECT mergins = new RECT;
	while (true)
	{
		MessageBeep(0xFFFFFFFF);
		std::thread popUp(genPopUpBox, msg, title, MB_HELP);
		HWND msgHandle;
		size_t counter = 0;
		Sleep(400);
		do
		{
			Sleep(350);
			msgHandle = FindWindowA(NULL, title.c_str());
			counter++;
		} while (!msgHandle && counter < 10);
		GetWindowRect(msgHandle, mergins);
		MoveWindow(msgHandle, uniX(rng), uniY(rng), (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
		std::thread temp = movePopUp(msgHandle, msg, title, screenSize);
		popUp.join();
		Sleep(20000);
		std::string bye = "Bye Bye ";
		CloseWindow(FindWindowA(NULL, (bye+title).c_str()));
		temp.join();
	}
}

HWND genPopUpBox(std::string msg, std::string title, long mode)
{
	HWND temp = 0;
	MessageBox(temp, msg.c_str(), title.c_str(), mode);
	return temp;
}


std::thread movePopUp(HWND handle, std::string msg, std::string title, RECT screenSize)
{
	LPRECT mergins = new RECT;
	LPPOINT cursorPos = new POINT;
	POINT winPosBottomRight = {0, 0};
	POINT winPosBottomLeft = { 0, 0 };
	POINT winPosTopRight = { 0, 0 };
	POINT winPosTopLeft = { 0, 0 };
	while (GetWindowRect(handle, mergins))
	{
		BringWindowToTop(handle);
		winPosBottomRight = { mergins->right, mergins->bottom };
		winPosBottomLeft = { mergins->left, mergins->bottom };
		winPosTopRight = { mergins->right, mergins->top };
		winPosTopLeft = { mergins->left, mergins->top };
		GetCursorPos(cursorPos);
		calcAndMoveWindow(handle, mergins, cursorPos, winPosBottomRight, winPosBottomLeft, winPosTopRight, winPosTopLeft, screenSize);
	} 
	std::string _title = "Bye Bye ";
	std::thread popUp(genPopUpBox, "I will be back...", _title+title, MB_ICONWARNING);
	return popUp;
}

void calcAndMoveWindow(HWND handle, LPRECT mergins, LPPOINT cursorPos, POINT winPosBottomRight, POINT winPosBottomLeft, POINT winPosTopRight, POINT winPosTopLeft, RECT screenSize)
{
	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> uniX(0, screenSize.right);
	std::uniform_int_distribution<int> uniY(0, screenSize.bottom);

	if (abs(winPosBottomLeft.x - cursorPos->x) < SENSITIVITY && abs(winPosBottomLeft.y - cursorPos->y) < SENSITIVITY)
	{
		MoveWindow(handle, winPosTopLeft.x + REACTION, winPosTopLeft.y - REACTION, (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}
	else if (abs(winPosTopLeft.x - cursorPos->x) < SENSITIVITY && abs(winPosTopLeft.y - cursorPos->y) < SENSITIVITY)
	{
		MoveWindow(handle, winPosTopLeft.x + REACTION, winPosTopLeft.y + REACTION, (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}
	else if (abs(winPosTopRight.x - cursorPos->x) < SENSITIVITY && abs(winPosTopRight.y - cursorPos->y) < SENSITIVITY)
	{
		MoveWindow(handle, winPosTopLeft.x - REACTION, winPosTopLeft.y + REACTION, (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}
	else if (abs(winPosBottomRight.x - cursorPos->x) < SENSITIVITY && abs(winPosBottomRight.y - cursorPos->y) < SENSITIVITY)
	{
		MoveWindow(handle, winPosTopLeft.x - REACTION, winPosTopLeft.y - REACTION, (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}
	else if (abs(cursorPos->y - winPosBottomLeft.y) < SENSITIVITY + 12 && (0 < winPosBottomRight.x - cursorPos->x && winPosBottomRight.x - cursorPos->x < (mergins->right - mergins->left)))
	{
		MoveWindow(handle, winPosTopLeft.x, winPosTopLeft.y - REACTION, (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}
	else if (abs(cursorPos->x - winPosBottomLeft.x) < SENSITIVITY && (0 < winPosBottomRight.y - cursorPos->y && winPosBottomRight.y - cursorPos->y < (mergins->bottom - mergins->top)))
	{
		MoveWindow(handle, winPosTopLeft.x + REACTION, winPosTopLeft.y, (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}
	else if (abs(cursorPos->y - winPosTopLeft.y) < SENSITIVITY && (0 < winPosBottomRight.x - cursorPos->x && winPosBottomRight.x - cursorPos->x < (mergins->right - mergins->left)))
	{
		MoveWindow(handle, winPosTopLeft.x, winPosTopLeft.y + REACTION, (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}
	else if (abs(cursorPos->x - winPosTopRight.x) < SENSITIVITY && (0 < winPosBottomRight.y - cursorPos->y && winPosBottomRight.y - cursorPos->y < (mergins->bottom - mergins->top)))
	{
		MoveWindow(handle, winPosTopLeft.x - REACTION, winPosTopLeft.y, (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}

	if (winPosBottomLeft.x < 0 || winPosBottomRight.x > screenSize.right || winPosBottomLeft.y > screenSize.bottom || winPosTopLeft.y < 0)
	{
		MoveWindow(handle, uniX(rng), uniY(rng), (mergins->right - mergins->left), (mergins->bottom - mergins->top), TRUE);
	}
}
